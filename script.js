let potential = [5, 5, 5];
let requirement = [15, 15, 15];
let deadline = {
    data: "2023-02-28",
}

function arraySum(points) {
    let sum = 0;
    for (let i = 0; i < points.length; i++) {
        sum = sum + points[i];
    }
    return sum;
}

function count() {
    let potentialSum = arraySum(potential);
    let requirementSum = arraySum(requirement);
    let differenceDays = requirementSum / potentialSum
    let today = (new Date()).toISOString().slice(0, 10);
    let yearToday = today.slice(0, 4);
    let monthToday = today.slice(5, 7);
    let dayToday = today.slice(8, 10);
    let yearDeadline = deadline.data.slice(0, 4);
    let monthDeadline = deadline.data.slice(5, 7);
    let dayDeadline = deadline.data.slice(8, 10);
    let todayTotal = yearToday * 365 + monthToday * 31 + dayToday;
    let deadlineTotal = yearDeadline * 365 + monthDeadline * 31 + dayDeadline;
    let fulfillment = deadlineTotal - todayTotal
    let differenceTotal = fulfillment - differenceDays;
    let notGet = 0;
    if (differenceTotal < 0) {
        notGet = differenceTotal * 8 * -1
    }

    if (fulfillment > differenceDays) {
        alert(`Усі завдання будуть успішно виконані за ${differenceTotal} днів до настання дедлайну!`)
    } else {

        alert(`Команді розробників доведеться витратити додатково ${notGet} годин після дедлайну, щоб виконати всі завдання в беклозі`)
    }
}

count()



